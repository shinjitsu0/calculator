import service.PnrExpressionToResult;
import service.ExpressionToPnrParser;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println(
                "Введите выражение в одну строку, допустимые символы \"+ - * / () .\",\n" +
                        "не допускается ввод двух символов подряд, для ввода отрицательного значения помещяйте его в (),\n" +
                        "для ввода числа с плавающей запятой используйте \".\" в качестве разделителя,\n" +
                        "для выхода введите \"end\".");
        PnrExpressionToResult pnrExpressionToResult;
        while (true) {
            Scanner in = new Scanner(System.in);
            String expression = in.nextLine();
            if (expression.equals("end")) {
                break;
            }
            pnrExpressionToResult = new PnrExpressionToResult(new ExpressionToPnrParser(expression));
            System.out.println(pnrExpressionToResult.calculate());
        }
    }
}