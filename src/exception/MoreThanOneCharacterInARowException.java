package exception;

public class MoreThanOneCharacterInARowException extends IllegalArgumentException{
    public MoreThanOneCharacterInARowException(String message) {
        super(message);
    }
}
