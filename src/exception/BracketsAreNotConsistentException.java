package exception;

public class BracketsAreNotConsistentException extends IllegalArgumentException{
    public BracketsAreNotConsistentException(String message) {
        super(message);
    }
}
