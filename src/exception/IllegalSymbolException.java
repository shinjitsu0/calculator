package exception;

public class IllegalSymbolException extends IllegalArgumentException{
    public IllegalSymbolException(String message) {
        super(message);
    }
}
