package service;

import exception.DivideByZeroException;

import java.util.Stack;

public record PnrExpressionToResult(ExpressionToPnrParser expressionToPnrParser) {
    public Double calculate() {
        Stack<Double> stack = new Stack<>();
        for (String element : expressionToPnrParser.parse()) {
            switch (element) {
                case "+" -> stack.push(stack.pop() + stack.pop());
                case "-" -> {
                    Double a = stack.pop();
                    Double b = stack.pop();
                    stack.push(b - a);
                }
                case "*" -> stack.push(stack.pop() * stack.pop());
                case "/" -> {
                    Double a = stack.pop();
                    Double b = stack.pop();
                    if(a == 0) {
                        throw new DivideByZeroException("На ноль делить нельзя");
                    } else {
                        stack.push(b / a);
                    }
                }
                case "u-" -> stack.push(-stack.pop());
                default -> stack.push(Double.valueOf(element));
            }
        }
        return stack.pop();
    }
}
