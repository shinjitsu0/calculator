package service;

import exception.BracketsAreNotConsistentException;
import exception.IllegalSymbolException;
import exception.MoreThanOneCharacterInARowException;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpressionValidator {

    public boolean isValid(String expression) {
        String expressionWithoutSpaces = expression.replaceAll(" ", "");
        Stack<Character> stack = new Stack<>();
        Pattern forbiddenCharactersPattern = Pattern.compile("[^+\\-/*().\\d\\s+]");
        Pattern doubleOperatorsPattern = Pattern.compile("[+./*-]{2,}");
        Matcher forbiddenCharactersMatcher = forbiddenCharactersPattern.matcher(expressionWithoutSpaces);
        Matcher doubleOperatorsMatcher = doubleOperatorsPattern.matcher(expressionWithoutSpaces);
        if (forbiddenCharactersMatcher.find()) {
            throw new IllegalSymbolException("Выражение содержит недопустимый символ "
                    + expressionWithoutSpaces.substring(forbiddenCharactersMatcher.start()));
        }
        if (doubleOperatorsMatcher.find()) {
            throw new MoreThanOneCharacterInARowException("Выражение содержит два знака операции подряд ");
        }
        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == '(') {
                stack.push(expression.charAt(i));
            } else if (expression.charAt(i) == ')'){
                if (!stack.isEmpty()) {
                    stack.pop();
                } else {
                    throw new BracketsAreNotConsistentException("Скобки не согласованы");
                }
            }
        }
        if(!stack.isEmpty()) {
            throw new BracketsAreNotConsistentException("Скобки не согласованы");
        }
        return true;
    }
}
