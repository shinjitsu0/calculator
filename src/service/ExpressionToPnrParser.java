package service;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

public class ExpressionToPnrParser {
    ExpressionValidator expressionValidator = new ExpressionValidator();

    String expression;

    public ExpressionToPnrParser(String expression) {
        this.expression = expression;
    }

    private static final String UNARY_MINUS = "u-";
    private static final String OPERATORS = "() +-*/";

    private boolean isOperator(String token) {
        if (token.length() != 1) return false;
        for (int i = 0; i < OPERATORS.length(); i++) {
            if (token.charAt(0) == OPERATORS.charAt(i)) return true;
        }
        return false;
    }

    private int priority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }

    public List<String> parse() {
        expressionValidator.isValid(expression);
        List<String> resultList = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        StringTokenizer tokenizer = new StringTokenizer(expression, OPERATORS, true);
        String previousToken = "";
        String currentToken;
        while (tokenizer.hasMoreTokens()) {
            currentToken = tokenizer.nextToken();
            if (currentToken.equals(" ")) continue;
            if (isOperator(currentToken)) {
                if (currentToken.equals("(")) stack.push(currentToken);
                else if (currentToken.equals(")")) {
                    while (!stack.peek().equals("(")) {
                        resultList.add(stack.pop());
                    }
                    stack.pop();
                    if (!stack.isEmpty()) {
                        resultList.add(stack.pop());
                    }
                } else {
                    if (currentToken.equals("-") && (previousToken.equals("") || (isOperator(previousToken) && !previousToken.equals(")")))) {
                        currentToken = UNARY_MINUS;
                    } else {
                        while (!stack.isEmpty() && (priority(currentToken) <= priority(stack.peek()))) {
                            resultList.add(stack.pop());
                        }

                    }
                    stack.push(currentToken);
                }
            } else {
                resultList.add(currentToken);
            }
            previousToken = currentToken;
        }

        while (!stack.isEmpty()) {
            resultList.add(stack.pop());
        }
        return resultList;
    }
}